package com.example.kamill.systemy_osadzone_sqlite;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.example.kamill.systemy_osadzone_sqlite.Database.Helper;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private Helper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        db = new Helper(this, "Persons.db", null, 1);
        TextView peoplesCount = (TextView) findViewById(R.id.total_count);
        ListView listView = (ListView) findViewById(R.id.peoplesList);
        ArrayList<String> peoples = db.selectAll();
        ArrayAdapter<String> listAdapter =
                new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, peoples);

        peoplesCount.setText("" + db.count());
        listView.setAdapter(listAdapter);

        Button btnAdd = (Button) findViewById(R.id.btn_insert);
        Button btnDeleteAll = (Button) findViewById(R.id.btn_deleteAll);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText username = (EditText) findViewById(R.id.edit_text_name);
                db.insert(username.getText().toString());
                refresh();
            }
        });

        btnDeleteAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                db.deleteAll();
                refresh();
            }
        });


    }

    private void refresh() {
        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }

}
